#include <iostream>

using namespace std;

//--Natural Numbers Start.
int snatural(int integerReceiver) {
	int n = integerReceiver;
	int i = 0;
	int s = 0;

	for (i = 0; i <= n; i++) {
		s += i;
	}
	return s;
}

int xnatural() {
	int n = 0;
	int i = 0;
	cout << "How many NATURAL numbers want to see? ";
	cin >> n;

	for (i = 0; i <= n; i++) {
		cout << i << ", " << endl;
	}
	return 0;
}

void znatural(int integerReceiver) {
	int n = integerReceiver;
	int i = 0;

	for (i = 0; i <= n; i++) {
		cout << i << ", " << endl;
	}
}
//--Natural Numbers End

//--Polygon angles' sum Start
int xpolygon(int integerReceiver) {
	int n = integerReceiver;
	if (n < 3) {
		return 0;
	}
	else {
		return (integerReceiver - 2) * 180;
	}
}
//--Polygon angles' sum End

//--Factorial Start
void xfactorial() {
	int n;
	long f = 1;
	cout << "Please input the integer to get the factorial: ";
	cin >> n;

	while (n < 0) {
		cout << "Input value should not be negative. Input again: ";
		cin >> n;
	}

	if (n == 0) {
		cout << "Factorial of '" << n << "!' is '" << f << "'" << endl;
	}
	else {
		int i = n;
		while (i > 1) {
			f = f * i;
			--i;
		}
		cout << "Factorial of '" << n << "!' is '" << f << "'" << endl;
	}
}

void yfactorial() {
	int n;
	long f = 1;
	cout << "Please input the integer to get the factorial: ";
	cin >> n;

	while (n < 0) {
		cout << "Input value should not be negative. Input again: ";
		cin >> n;
	}

	if (n > 0) {
		int i = 1;
		while (i <= n) {
			f = f * i;
			++i;
		}
		cout << "Factorial of '" << n << "!' is '" << f << "'" << endl;
	}
	else {
		cout << "Factorial of '" << n << "!' is '" << f << "'" << endl;
	}
}

long zfactorial(int integerReceiver) {
	int n = integerReceiver;
	long f = 1;
	cout << "Please input the integer to get the factorial: ";
	cin >> n;

	while (n < 0) {
		cout << "Input value should not be negative. Input again: ";
		cin >> n;
	}

	if (n > 0) {
		int i = 1;
		while (i <= n) {
			f = f * i;
			++i;
		}
		return f;
	}
	else {
		return f;
	}
}
//--Factorial End