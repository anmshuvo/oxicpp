#include <iostream>

using namespace std;

/*
Switch case only work with Integer or Character.
*/

void iswitch() {
	int n;
	cout << "Pleasse input an Integer value: ";
	cin >> n;

	switch (n) {
	default: cout << "Your input is not one/two/three." << endl;
		break;
	case 1: cout << "You input one" << endl;
		break;
	case 2: cout << "You input two" << endl;
		break;
	case 3: cout << "You input three" << endl;
		break;
	}
}

void cswitch() {
	char n;
	cout << "Pleasse input an Character value: ";
	cin >> n;

	switch (n) {
	default: cout << "Your input is not a/b/c." << endl;
		break;
	case 1: cout << "You input a" << endl;
		break;
	case 2: cout << "You input b" << endl;
		break;
	case 3: cout << "You input c" << endl;
		break;
	}
}