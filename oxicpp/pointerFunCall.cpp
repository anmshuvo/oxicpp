#include <iostream>

using namespace std;

void cube(int* pRec) {
	*pRec = *pRec * *pRec * *pRec;
	cout << *pRec << endl;
}