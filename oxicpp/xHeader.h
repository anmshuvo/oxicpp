// switchCaseOperator
void iswitch();
void cswitch();

// xMath
int snatural(int);
int xnatural();
void znatural(int);
int xpolygon(int);
void xfactorial();
void yfactorial();
long zfactorial(int);


// printFunctions
int print(int);
double print(double);
float print(float);
void printArray(int*, int);
//void printArray(int [], int);

//


int naturalNumbers();
int naturalNumbersSum();
int perfectNumbers();
void cube(int*);