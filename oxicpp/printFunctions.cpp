#include <iostream>
#include <string>

using namespace std;

int print(int printReceiver) {
	cout << printReceiver << endl;
	return 0;
}

double print(double printReceiver) {
	cout << printReceiver << endl;
	return 0;
}

float print(float printReceiver) {
	cout << printReceiver << endl;
	return 0;
}
/*
string print(string printReceiver) {
	cout << printReceiver << endl;
	return 0;
}
*/

/*
Using pointer.
*/
void printArray(int* printReceiver, int arraySizeReceiver) {
	for (int i = 0; i < arraySizeReceiver; ++i) {
		cout << *(printReceiver + i) << ", ";
	}
}

/*
In array, content of array arr[i] equivalent i[arr]. It works like *(arr + i) or *(i + arr).
In array, address of array (arr + i) equivalent &(arr[i]).

void printArray(int printReceiver[], int arraySizeReceiver) {
	for (int i = 0; i < arraySizeReceiver; ++i) {
		cout << printReceiver[i] << ", ";
	}
}
*/

