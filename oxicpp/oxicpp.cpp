// oxicpp.cpp : This file contains the 'main' function. Program execution begins and ends there.


#include <array> 
#include <cstring>
#include <iostream>
#include <string>
#include "xHeader.h"

using namespace std;

int main()
{
	//iswitch();
	//cswitch();
	
	//xfactorial();
	//yfactorial();
	//cout << zfactorial(5) << endl;
	
	//int arr[5] = { 1,2,3,4,5 };
	//printArray(arr, 5);

	//print(xpolygon(4));

	//--Reference Variable 
/*
	int testVariable = 10;
	int &refVariable = testVariable; // use '&' to reference variable.

	refVariable = refVariable + 10;
	cout << refVariable << endl;
*/

	//xnatural();
	//znatural(10);
	print(snatural(10));
	//naturalNumbersSum();
	//perfectNumbers();

	


	//--Pointer
/*
	int temp1 = 360, temp2 = 400;
	//cout << "Memory Location Temp1: " << (unsigned) &temp1 << endl; // Use '&' to see the memory location of RAM. Use 'unsigned' keyword for view in Integer value instead of a Hexadecimal value. 
	//cout << "Memory Location Temp2: " << (unsigned) &temp2 << endl;

	int* t1, * t2; // Use '*' to define pointer variable.
	t1 = &temp1;
	t2 = &temp2;

	//cout << "Value of t1: " << (unsigned) t1 << endl;
	//cout << "Value of t2: " << (unsigned) t2 << endl;
	//
	//cout << "Content Value of t1: " << *t1 << endl;
	//cout << "Content Value of t2: " << *t2 << endl;

	t2 = t2 + 1; // Change the memory location.

	//cout << "Value of t1: " << (unsigned)t1 << endl;
	//cout << "Value of t2: " << (unsigned)t2 << endl;
	//
	//cout << "Size of Pointer t1: " << sizeof(t1) << endl;
	//cout << "Size of Pointer t2: " << sizeof(t2) << endl;
	//cout << "Size of content t1: " << sizeof(*t1) << endl;
	//cout << "Size of content t2: " << sizeof(*t2) << endl;

	double* d;
	//cout << "Size of Pointer d: " << sizeof(d) << ", Size of Content d: " << sizeof(*d) << endl;

	char* k = (char *)t2; // Assign Integer pointer to a Character pointer.
	//cout << "*k is: " << *k << endl;

	int a = 5;
	cube(&a);

*/
	//--Array
/*
	int frr[10];
	int n = 0;

	cout << "Input how many numbers? ";
	cin >> n;

	while (n <= 0 || n > 10) {
		cout << "Given number must be 1 - 10" << endl;
		cout << "Input number again: ";
		cin >> n;
	}

	for (int i = 0; i < n; ++i) {
		cout << "Input the [" << i + 1 << "] number: ";
		cin >> frr[i];
	}

	for (int j = 0; j < n; ++j) {
		cout << frr[j] << ", ";
	}

*/
/*
	int primeNumber[] = { 2, 3, 5, 7, 11 }; // We can declare an array as like.
	
	for (int i = 0; i < 5; ++i) {
		cout << primeNumber[i] << endl;
	}
*/
/*
	int poArray[] = { 5, 10, 15 }; // Relation with Array and Pointer.
	
	printA(poArray, 3);
	//cout << poArray[1] << ": " << *(poArray + 1) << ": " << 1[poArray] << ": " << (poArray + 1);
*/
/**/
//	int rangeArray[] = { 1, 2, 3, 4, 5 }; // Range For Loop example.
//	cout << rangeArray.size();
	
//	for (int i : rangeArray) {
//		cout << i << ", ";
//	}
//
//	int getAbsSum(std::vector<int> arr) {
//		for (int i = 0; i < (arr.size() - 1); ++i) {
//			int xT = arr[i] + xT;
//		}
//		return abs(xT);
//	}
//
	//--String
/**/
	//string name;

	//cout << "Please input your name: ";
	//getline(cin, name); // To capture all line including space use getline function/method.

	//cout << "Welcome! " << name << "!" << endl;
	//cout << name.length() << ": " << name.at(1) << endl; // .at() use for the character index. .length() or .size() use for the total caracter. 
	//cout << name.size() << ": " << name.at(1) << endl;
	//
	//cout << name[1] << ": " << name.at(1) << endl; // Alternetly we can use [] for .at()

	/* ASCII value of upper case A is 65 and lower case a is 97. The gap is 32 between upper and lower. */
	/*Make all upper case*/
/*
	for (int i = 0; i <= name.length(); ++i) {
		if (name[i] >= 'a' && name[i] <= 'z') {
			name[i] -= 32;
		}
	}
	cout << "Welcome! " << name << "!" << endl;

	/*Make all lower case
	for (char &i : name) {
		if (i >= 'A' && i <= 'Z') {
			i += 32;
		}
		cout << i << endl;
	}
	cout << "Welcome! " << name << "!" << endl;
*/


	//string name(15, '@'); // String class Constructors
	//cout << name << endl;

/*
	string str = "Hello";
	string::iterator it = str.begin();
	while(it != str.end()){
	   //cout << *it << ", ";
	   if (*it >= 'a' && *it <= 'z'){
		*it -= 32;
	   }
	   ++it;
	}

	string::reverse_iterator rit = str.rbegin();
	while (rit != str.rend()) {
		cout << *rit << ", ";
		++rit;
	}

*/
/*
	string someT = "Sadiqur Rahman";
	string::iterator itt = someT.begin();

	while (itt != someT.end()) {
		cout << *itt << endl; // dereferencing operator * use for the character
		++itt;
	}

	string::reverse_iterator ritt = someT.rbegin();

	while (ritt != someT.rend()) {
		cout << *ritt << endl; // dereferencing operator * use for the character
		++ritt;
	}
*/

	return 0;
}


