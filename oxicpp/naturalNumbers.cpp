#include <iostream>

using namespace std;

// First n Natural Numbers.

int naturalNumbers() {
	int n = 0;
	int i = 0;
	cout << "How many NATURAL numbers want to see? " << endl;
	cin >> n;
	
	for (i = 0; i <= n; i++) {
		cout << i << ", ";
	}
	return 0;
}

// First n Natural Numbers.

int naturalNumbersSum() {
	int n = 0;
	int i = 0;
	int sum = 0;
	cout << "Pass a number for view sum of NATURAL number? " << endl;
	cin >> n;

	for (i = 1; i <= n; i++) {
		sum = sum + i;
	}
	cout << "Sum of first " << n << " NATURAL numbers is: " << sum;
	return 0;
}